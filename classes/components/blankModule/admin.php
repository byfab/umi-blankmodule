<?php
	/**
	 * Класс функционала административной панели
	 */
	class BlankModuleAdmin {

		use baseModuleAdmin;
		/**
		 * @var blankModule $module
		 */
		public $module;

		/**
		 * Возвращает список страниц
		 * @return bool
		 * @throws coreException
		 * @throws selectorException
		 */
		public function category() {
			$this->setDataType('list');
			$this->setActionType('view');

			if ($this->module->ifNotXmlMode()) {
				$this->setDirectCallError();
				$this->doData();
				return true;
			}

			$limit = getRequest('per_page_limit');
			$pageNumber = (int) getRequest('p');
			$offset = $limit * $pageNumber;

			$pages = new selector('pages');
			$pages->types('object-type')->name($this->module->moduleName, 'category');
			$pages->types('object-type')->name($this->module->moduleName, 'item');
			$pages->limit($offset, $limit);

			selectorHelper::detectHierarchyFilters($pages);
			selectorHelper::detectWhereFilters($pages);
			selectorHelper::detectOrderFilters($pages);

			$result = $pages->result();
			$total = $pages->length();

			$this->setDataRange($limit, $offset);
			$data = $this->prepareData($result, 'pages');
			$this->setData($data, $total);
			$this->doData();
		}

		public function objects() {
			$this->setDataType('list');
			$this->setActionType('view');

			if ($this->module->ifNotXmlMode()) {
				$this->setDirectCallError();
				$this->doData();
				return true;
			}

			$limit = getRequest('per_page_limit');
			$curr_page = (int) getRequest('p');
			$offset = $limit * $curr_page;

			$objects = new selector('objects');
			$objects->types('object-type')->name('dummy', 'object');
			$objects->limit($offset, $limit);
			selectorHelper::detectFilters($objects);

			$result = $objects->result();
			$total = $objects->length();

			$this->setDataRange($limit, $offset);
			$data = $this->prepareData($result, 'objects');
			$this->setData($data, $total);
			$this->doData();
		}

		/**
		 * Возвращает настройки модуля.
		 * Если передан ключевой параметр $_REQUEST['param0'] = do, то сохраняет настройки.
		 * @throws coreException
		 */
		public function config() {
			$groupKey = 'paging';
			$pagesKey = 'int:pages_per_page';
			$objectsKey = 'int:objects_per_page';

			$params = [
				$groupKey => [
					$pagesKey	=> null,
					$objectsKey	=> null
				]
			];

			$umiRegistry = regedit::getInstance();
			$pagesLimitXpath = $this->module->pagesLimitXpath;
			$objectsLimitXpath = $this->module->objectsLimitXpath;

			if (getRequest('param0') == 'do') {
				$params = $this->expectParams($params);
				$umiRegistry->setVar($pagesLimitXpath, $params[$groupKey][$pagesKey]);
				$umiRegistry->setVar($objectsLimitXpath, $params[$groupKey][$objectsKey]);
				$this->chooseRedirect();
			}

			$params[$groupKey][$pagesKey] = $umiRegistry->getVal($pagesLimitXpath);
			$params[$groupKey][$objectsKey] = $umiRegistry->getVal($objectsLimitXpath);

			$this->setDataType('settings');
			$this->setActionType('modify');
			$data = $this->prepareData($params, 'settings');
			$this->setData($data);
			$this->doData();
		}

		public function addElement() {
			$parent = $this->expectElement("param0");
			$type = (string) getRequest("param1");
			$mode = (string) getRequest("param2");

			$this->setHeaderLabel("header-" . $this->module->moduleName . "-add-" . $type);

			$inputData = Array(
				"type" => $type,
				"parent" => $parent,
				'type-id' => getRequest('type-id'),
				"allowed-element-types" => Array($type)
			);

			if($mode == "do") {
				$element_id = $this->saveAddedElementData($inputData);
				$this->chooseRedirect("{$this->pre_lang}/admin/{$this->module->moduleName}/editElement/{$element_id}/");
			}

			$this->setDataType("form");
			$this->setActionType("create");

			$data = $this->prepareData($inputData, "page");

			$this->setData($data);
			return $this->doData();
		}

		/**
		 * Возвращает данные для построения формы редактирования страницы модуля.
		 * Если передан ключевой параметр $_REQUEST['param1'] = do, то сохраняет изменения страницы.
		 * @throws coreException
		 * @throws expectElementException
		 * @throws wrongElementTypeAdminException
		 */
		public function editElement() {
			$element = $this->expectElement('param0', true);
			$mode = (string)getRequest('param1');

			$tList = umiHierarchyTypesCollection::getInstance()->getTypesList();
			$this->setHeaderLabel("header-" . $this->module->moduleName . "-edit-" . $this->getObjectTypeMethod($element->getObject()));

			$allowedTypes = array();
			foreach ($tList as $type) {
				if ($type->getName() == $this->module->moduleName) $allowedTypes[] = $type->getExt();
			}
			$inputData = Array('element' => $element, 'allowed-element-types' => $allowedTypes);

			if ($mode == "do") {
				$element = $this->saveEditedElementData($inputData);
				$this->chooseRedirect();
			}

			$this->setDataType("form");
			$this->setActionType("modify");

			$data = $this->prepareData($inputData, 'page');

			$this->setData($data);
			return $this->doData();
		}

		/**
		 * Возвращает настройки табличного контрола
		 * @param string $param контрольный параметр
		 * @return array
		 * @throws publicAdminException
		 */
		public function getDatasetConfiguration($params) {
			$params		= explode (',' , $params);
			$moduleName = $this->module->moduleName;
			$eType		= $params[0];
			$hType		= $params[1];
			$loadMethod	= $params[2];

			$typeId	= umiObjectTypesCollection::getInstance()->getBaseType($moduleName, $hType);

			switch($eType) {
				case 'pages' :
					$moduleName		= 'content';
					$delMethod		= 'tree_delete_element';
					$activityMethod	= 'tree_set_activity';
					$defaults	= array();
					break;
				case 'objects' :
					$delMethod		= 'delObject';
					$activityMethod	= 'activityObject';
					$defaults	= array();
					break;
				case 'types' :
					$moduleName = 'data';
					$delMethod  = 'type_del';
					$activityMethod	= 'activity';
					$defaults	= array();
					break;
				default:
					break;
			}
			return array(
				'methods' => array(
					array('title'=>getLabel('smc-load'),		'module'=>$moduleName,	'#__name'=>$loadMethod, 'forload'=>true),
					array('title'=>getLabel('smc-delete'),		'module'=>$moduleName,	'#__name'=>$delMethod, 'aliases' => 'tree_delete_element,delete,del'),
					array('title'=>getLabel('smc-activity'),	'module'=>$moduleName,	'#__name'=>$activityMethod, 'aliases' => 'tree_set_activity,activity'),
					array('title'=>getLabel('smc-copy'),		'module'=>'content',	'#__name'=>'tree_copy_element')),
				'types' => array(
					array('common' => 'true', 'id' => $typeId)
				),
				'stoplist' => array(''),
				'default' => $defaults
			);
		}

	}
?>