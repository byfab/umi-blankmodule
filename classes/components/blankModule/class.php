<?php
	/**
	 * Болванка модуля
	 * @link http://api.docs.umi-cms.ru/razrabotka_nestandartnogo_funkcionala/razrabotka_sobstvennyh_makrosov_i_modulej/sozdanie_modulya/
	 */
	class blankModule extends def_module {

		public $moduleName = 'blankModule';

		/**
		 * @var string $pagesLimitXpath путь до опции реестра, отвечающего за ограничение количества выводимых страниц
		 */
		public $pagesLimitXpath;
		/**
		 * @var string $objectsLimitXpath путь до опции реестра, отвечающего за ограничение количества выводимых объектов
		 */
		public $objectsLimitXpath;

		/**
		 * Конструктор
		 */
		public function __construct() {
			parent::__construct();

			$this->pagesLimitXpath = '//modules/' . $this->moduleName . '/paging/pages';
			$this->objectsLimitXpath = '//modules/' . $this->moduleName . '/paging/objects';

			if (cmsController::getInstance()->getCurrentMode() == "admin") {
				$this->initTabs();
				$this->includeAdminClasses();
			} else {
				$this->includeGuestClasses();
			}

			$this->includeCommonClasses();
		}

		/**
		 * Возвращает ссылки на форму редактирования страницы модуля и
		 * на форму добавления дочернего элемента к странице.
		 * @param int $element_id идентификатор страницы модуля
		 * @param string|bool $element_type тип страницы модуля
		 * @return array
		 */
		public function getEditLink($element_id, $element_type = false) {
			return [
				false,
				$this->pre_lang . "/admin/". $this->moduleName . "/editElement/{$element_id}/"
			];
		}

		/**
		 * Возвращает ссылку на редактирование объектов в административной панели
		 * @param int $objectId ID редактируемого объекта
		 * @param string|bool $type метод типа объекта
		 * @return string
		 */
		public function getObjectEditLink($objectId, $type = false) {
			return $this->pre_lang . "/admin/". $this->moduleName . "/editObject/"  . $objectId . "/";
		}

		/**
		 * Создает вкладки административной панели модуля
		 */
		protected function initTabs() {
			$configTabs = $this->getConfigTabs();

			if ($configTabs instanceof iAdminModuleTabs) {
				$configTabs->add("config");
			}

			$commonTabs = $this->getCommonTabs();

			if ($commonTabs instanceof iAdminModuleTabs) {
				$commonTabs->add('category');
				//$commonTabs->add('objects');
			}
		}

		/**
		 * Подключает классы функционала административной панели
		 */
		protected function includeAdminClasses() {
			$this->__loadLib("admin.php");
			$this->__implement(ucfirst($this->moduleName) . "Admin");

			$this->loadAdminExtension();

			$this->__loadLib("customAdmin.php");
			$this->__implement(ucfirst($this->moduleName) . "CustomAdmin", true);
		}

		/**
		 * Подключает классы функционала клиентской части
		 */
		protected function includeGuestClasses() {
			$this->__loadLib("macros.php");
			$this->__implement(ucfirst($this->moduleName) . "Macros");

			$this->loadSiteExtension();

			$this->__loadLib("customMacros.php");
			$this->__implement(ucfirst($this->moduleName) . "CustomMacros", true);
		}

		/**
		 * Подключает общие классы функционала
		 */
		protected function includeCommonClasses() {
			$this->loadCommonExtension();
			$this->loadTemplateCustoms();
		}
	};
?>
