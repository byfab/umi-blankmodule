<?php
	/**
	 * Языковые константы для русской версии
	 */
	$i18n = [
		'module-blankModule'			=> 'Пустой Модуль',
		'header-blankModule-pages'		=> 'Страницы',
		'header-blankModule-objects'	=> 'Объекты',
		'header-blankModule-config'		=> 'Настройки',
		'header-blankModule-addPage'	=> 'Создание страницы',
		'header-blankModule-editPage'	=> 'Редактирование страницы',
		'header-blankModule-addObject'	=> 'Создание объекта',
		'header-blankModule-editObject'	=> 'Редактирование объекта',
		'group-paging'				=> 'Настройки постраничного вывода',
		'option-pages_per_page'		=> 'Количество выводимых страниц на один лист',
		'option-objects_per_page'	=> 'Количество выводимых объектов на один лист',
		'label-add-page'			=> 'Создать страницу',
		'label-add-object'			=> 'Создать объект',
		'error-config-not-found'	=> 'Настройки не найдены',
		'error-page-not-found'		=> 'Страница не найдена',
	];
?>