<?php
/**
 * Установщик модуля PHP 7+
 */

$module_name = 'blankModule';
$module_path = './classes/components/' . $module_name;

/**
 * @var array $INFO реестр модуля
 */
$INFO = [
	'name' => $module_name,                             // Имя модуля
	'config' => '1',                                    // У модуля есть настройки
	'default_method' => 'item',                         // Метод по умолчанию в клиентской части
	'default_method_admin' => 'category',               // Метод по умолчанию в административной части
	'func_perms' => 'Группы прав на функционал модуля', // Группы прав
	'func_perms/guest' => 'Гостевые права',             // Гостевая группа прав
	'func_perms/admin' => 'Административные права',     // Административная группа прав
	'paging/' => 'Настройки постраничного вывода',      // Группа настроек
	'paging/pages' => 25,                               // Настройка количества выводимых страниц
	'paging/objects' => 25,                             // Настройка количества выводимых объектов
];

/**
 * @var array $COMPONENTS файлы модуля
 */
$COMPONENTS = [
	$module_path . '/lang.php',         // Языковые константы
	$module_path . '/i18n.php',         // Язык по умолчанию
	$module_path . '/i18n.en.php',      // Английский язык
	$module_path . '/permissions.php',  // Права
	$module_path . '/admin.php',        // Макросы административной части
	$module_path . '/customAdmin.php',
	$module_path . '/class.php',        // Класс модуля
	$module_path . '/macros.php',       // Макросы клиентской части
	$module_path . '/customMacros.php',
];
?>