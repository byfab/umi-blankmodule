<?php
	class blankModule extends def_module {
		public $per_page;
	
		public function __construct() {
			parent::__construct();
			//конструктор все по стандарту
			if(cmsController::getInstance()->getCurrentMode() == "admin") {
				$this->__loadLib("__admin.php");
				$this->__implement("__blankModule");
				$this->__loadLib("__custom_adm.php");
				$this->__implement("__custom_adm_blankModule");

				// Вкладки
				if($commonTabs = $this->getCommonTabs()) {
					$commonTabs->add("category", array("editElement", "addElement"));
					$commonTabs->add("objects", array("editObject", "addObject"));
					$commonTabs->add("types", array("editType", "addType"));
				}
				// -- Вкладки
			} else {
				$this->__loadLib("macros.php");
				$this->__implement("macros_blankModule");
				$this->__loadLib("__custom.php");
				$this->__implement("__custom_blankModule");

				$this->per_page = regedit::getInstance()->getVal("//modules/blankModule/per_page");
			}
		}


		//Выводит ссылку для быстрого редактирования обьектов в SMC
		public function getEditLink($element_id = false, $element_type = false) {
			$element = umiHierarchy::getInstance()->getElement($element_id);
			if (!$element) return false;
			$moduleName = get_class($this);
			//try $moduleName = __CLASS__;
			$parent_id = $element->getParentId();
			// Зависимости при добовлении потомков через SMC
			switch($element_type) {
				case "category": {
					$element_type = "item";
					break;
				}
				case "item": {
					$element_type = false;
					break;
				}
				default: {
					
				}
			}		
			$link_add = ($element_type) ? $this->pre_lang . "/admin/" . $moduleName . "/addElement/{$element_id}/{$element_type}/" : false;
			$link_edit = $this->pre_lang . "/admin/" . $moduleName . "/editElement/{$element_id}/";
			return Array($link_add, $link_edit);
		}

		public function getObjectEditLink($objectId, $type) {
			$moduleName = get_class($this);
			//try $moduleName = __CLASS__;
			return $this->pre_lang . "/admin/" . $moduleName . "/editObject/" . $objectId . "/";
		}

		public function getObjectTypeEditLink($typeId) {
			$moduleName = get_class($this);
			return Array(
				'create-link' => $this->pre_lang . "/admin/" . $moduleName . "/addType/{$typeId}/",
				'edit-link'   => $this->pre_lang . "/admin/" . $moduleName . "/editType/{$typeId}/"
			);
		}
/*		Для редактирования типов TODO включить в реализацию выше (класс + шаблоны)
		public function getEditLink($type_id) {
			$link_add = false;
			$link_edit = $this->pre_lang . "/admin/data/type_edit/{$type_id}/";
			return Array($link_add, $link_edit);
		}
*/
	};
?>