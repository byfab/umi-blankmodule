<?php

$i18n = Array(
	"module-blankModule"		=> "Пустой модуль",
	"header-blankModule-config"		=> "Настройки модуля",
	
	
	"label-edit-type-common"		=> "Свойства типа",
	"label-type-name"				=> "Название типа",
	"label-hierarchy-type"			=> "Назначение типа",
	"label-is-guide"				=> "Можно использовать как справочник",
	"label-is-public"				=> "Общедоступный",

	


	// from data module
	'js-trash-confirm-cancel'			=> 'Отменить',
	'js-trash-confirm-ok'				=> 'Очистить',
	'js-trash-restore'					=> 'Восстановить',
	'js-data-add-field'					=> 'Добавить поле',
	'js-data-edit-field'				=> 'Сохранить',
	'js-type-edit-edit'					=> 'изменить',
	'js-type-edit-remove'				=> 'удалить',
	'js-type-edit-add_group'			=> 'Добавить группу',
	'js-type-edit-add_field'			=> 'Добавить поле',
	'js-type-edit-title'				=> 'Название',
	'js-type-edit-name'					=> 'Идентификатор',
	'js-type-edit-tip'					=> 'Подсказка',
	'js-type-edit-type'					=> 'Тип',
	'js-type-edit-restriction'			=> 'Формат значения',
	'js-type-edit-guide'				=> 'Справочник',
	'js-type-edit-visible'				=> 'Видимое',
	'js-type-edit-required'				=> 'Обязательное',
	'js-type-edit-indexable'			=> 'Индексируемое',
	'js-type-edit-filterable'			=> 'Фильтруемое',
	'js-type-edit-important'			=> 'Важное',
	'js-type-edit-saving'				=> 'Сохранение',
	'js-type-edit-new_group'			=> 'Новая группа',
	'js-type-edit-new_group-title'		=> 'Добавление новой группы',
	'js-type-edit-new_field'			=> 'Новое поле',
	'js-type-edit-confirm_title'		=> 'Подтверждение удаления',
	'js-type-edit-confirm_text'			=> 'Если вы уверены, нажмите "Удалить" (действие необратимо).',

	'restriction-error-common'			=> 'Неверное значение',
	'restriction-error-discount-size'	=> 'Размер скидки должен находиться в диапозоне от 0 до 100%',
	'restriction-error-domain-id'		=> 'Домена с таким id нет в системе',
	'restriction-error-email'			=> 'Неверный e-mail',
	'restriction-error-object-type'		=> 'Должен быть передан существующий тип данных',

	'error-value-required'				=> 'Поле "%s" обязательно должно быть заполнено',

	'js-group-creating-success' => 'Группа полей создана успешно!',
	'js-group-creating-title' => 'Добавление новой группы полей',
	'js-group-updating-success' => 'Группа обновлена успешно!',
	'js-group-updating-title' => 'Обновление группы',
	'js-field-creating-success' => 'Поле создано успешно!',
	'js-field-creating-title' => 'Создание поля',
	'js-field-creating-error' => 'Ошибка добавления поля!',
	'js-field-updating-success' => 'Поле обновлено успешно!',
	'js-field-updating-title' => 'Обновление поля',
	'js-error-message' => 'Текст ошибки:',
	'js-error-occurred' => 'Возникла ошибка:',
	'js-save-button' => 'Сохранить',
	'js-group-deleting-confirm' => 'Вы уверены что хотите удалить группу?',
	'js-group-deleting-title' => 'Удаление группы полей',
	'js-group-deleting-success' => 'Группа полей удалена успешно',
	'js-field-deleting-confirm' => 'Вы уверены что хотите удалить это поле?',
	'js-field-deleting-title' => 'Удалить это поле?',
	'js-group-deleting-question' => 'Удалить эту группу полей?',
	'js-add-button' => 'Добавить',	
	
);

?>