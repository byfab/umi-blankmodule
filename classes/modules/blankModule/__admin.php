<?php
	abstract class __blankModule extends baseModuleAdmin {

		public function category() {
			$this->setDataType("list");
			$this->setActionType("view");
			if($this->ifNotXmlMode()) return $this->doData();

			$limit = getRequest('per_page_limit');
			$curr_page = (int) getRequest('p');
			$offset = $limit * $curr_page;


			$sel = new selector('pages');
			$sel->types('object-type')->name(get_class($this), 'category');
			$sel->types('object-type')->name(get_class($this), 'item');
			$sel->limit($offset, $limit);
			selectorHelper::detectFilters($sel);

			//Завершаем вывод
			$data = $this->prepareData($sel->result, "pages");
			$this->setData($data, $sel->length);
			$this->setDataRangeByPerPage($limit, $curr_page);
			return $this->doData();
		}

		public function objects() {
			$this->setDataType("list");
			$this->setActionType("view");
			if($this->ifNotXmlMode()) return $this->doData();

			$limit = getRequest('per_page_limit');
			$curr_page = (int) getRequest('p');
			$offset = $limit * $curr_page;


			$sel = new selector('objects');
			$sel->types('object-type')->name(get_class($this), 'object');
			$sel->limit($offset, $limit);
			selectorHelper::detectFilters($sel);

			//Завершаем вывод
			$data = $this->prepareData($sel->result, "objects");
			$this->setData($data, $sel->length);
			$this->setDataRangeByPerPage($limit, $curr_page);
			return $this->doData();
		}

		public function types() {
			$typeNames = array('blanktype', 'anotherblanktype');

			$this->setDataType("list");
			$this->setActionType("view");
			if($this->ifNotXmlMode()) return $this->doData();

			$limit = getRequest('per_page_limit');
			$curr_page = (int) getRequest('p');

			$typesCollection = umiObjectTypesCollection::getInstance();
			$typeId = $typesCollection->getBaseType(get_class($this), reset($typeNames));
			$subTypesId = (isset($_REQUEST['rel'][0])) ? $typesCollection->getSubTypesList($_REQUEST['rel'][0], false, true) : $typesCollection->getSubTypesList($typeId);


			$aTypes = array();
			foreach ($subTypesId as $subTypeId) {
				$aTypes[$subTypeId] = $typesCollection->getType($subTypeId)->getName();
			}

			$data = $this->prepareData(array_keys(array_slice($aTypes, $limit * $curr_page, $limit, true)), "types");

			foreach ($typeNames as $typeName){
				$aBaseTypes[] = umiHierarchyTypesCollection::getInstance()->getTypeByName(get_class($this), $typeName);
			}
			$data['basetypes'] = $this->prepareData($aBaseTypes, "hierarchy_types");
			$this->setData($data, sizeof($subTypesId));
			$this->setDataRangeByPerPage($limit, $curr_page);
			return $this->doData();
		}

		public function config() {
			$regedit = regedit::getInstance();
			$moduleName = cmsController::getInstance()->getCurrentModule();
			$params = Array(
				"config" => Array(
					"int:per_page"	=> NULL
				)
			);

			$mode = getRequest("param0");

			if($mode == "do") {
				$params = $this->expectParams($params);
				$regedit->setVar("//modules/{$moduleName}/per_page", $params['config']['int:per_page']);
				$this->chooseRedirect();
			}

			$params['config']['int:per_page'] = (int) $regedit->getVal("//modules/{$moduleName}/per_page");

			$this->setDataType("settings");
			$this->setActionType("modify");

			$data = $this->prepareData($params, "settings");

			$this->setData($data);
			return $this->doData();
		}

		public function addElement() {
			$parent = $this->expectElement("param0");
			$type = (string) getRequest("param1");
			$mode = (string) getRequest("param2");
			
			$moduleName = cmsController::getInstance()->getCurrentModule();
			
			$this->setHeaderLabel("header-" . $moduleName . "-add-" . $type);

			$inputData = Array(
				"type" => $type,
				"parent" => $parent,
				'type-id' => getRequest('type-id'),
				"allowed-element-types" => Array($type)
			);
	
			if($mode == "do") {
				$element_id = $this->saveAddedElementData($inputData);
				$this->chooseRedirect("{$this->pre_lang}/admin/{$moduleName}/editElement/{$element_id}/");
			}
	
			$this->setDataType("form");
			$this->setActionType("create");
	
			$data = $this->prepareData($inputData, "page");
	
			$this->setData($data);
			return $this->doData();
		}

		public function addObject() {
			$type = (string) getRequest("param0");
			$mode = (string) getRequest("param1");

			$moduleName = cmsController::getInstance()->getCurrentModule();

			$this->setHeaderLabel("header-" . $moduleName . "-add-" . $type);

			$inputData = Array(
				"type" => $type,
				'type-id' => getRequest('type-id'),
				"allowed-element-types" => Array($type)
			);
	
			if($mode == "do") {
				$object = $this->saveAddedObjectData($inputData);
				$this->chooseRedirect("{$this->pre_lang}/admin/{$moduleName}/editObject/{$object->getId()}/");
			}

			$this->setDataType("form");
			$this->setActionType("create");

			$data = $this->prepareData($inputData, "object");

			$this->setData($data);
			return $this->doData();
		}

		public function addType() {
			$parent = (int) getRequest("param0");
			$type = (string) getRequest("param1");
			$mode = (string) getRequest("param2");
			$moduleName = cmsController::getInstance()->getCurrentModule();
			$this->setHeaderLabel("header-" . $moduleName . "-add-" . $type);
	
			if($mode == "do") {
				if(!isset($_REQUEST['data']['name']) || !strlen($_REQUEST['data']['name'])) $this->chooseRedirect($_SESSION['referer']);
				$oTypesColl = umiObjectTypesCollection::getInstance();
				$parent = ($parent) ? $parent : umiObjectTypesCollection::getInstance()->getBaseType($moduleName, $type);
				$typeId = $oTypesColl->addType($parent, $_REQUEST['data']['name']);
				$hierarchyType = umiHierarchyTypesCollection::getInstance()->getTypeByName($moduleName, $type);
				if ($hierarchyType instanceof umiHierarchyType){
					$oTypesColl->getType($typeId)->setHierarchyTypeId($hierarchyType->getId());
				}
				$editLink = $this->getObjectTypeEditLink($typeId);
				$this->chooseRedirect($editLink['edit-link']);
			}
			
			$this->setDataType("form");
			$this->setActionType("create");
			$data = $this->prepareData($parent, "type");
			$this->setData($data);
			return $this->doData();
		}

		public function editElement() {
			$element = $this->expectElement('param0', true);
			$mode = (string)getRequest('param1');
	
			$moduleName = cmsController::getInstance()->getCurrentModule();
			$tList = umiHierarchyTypesCollection::getInstance()->getTypesList();
			$this->setHeaderLabel("header-" . $moduleName . "-edit-" . $this->getObjectTypeMethod($element->getObject()));
	
			$allowedTypes = array();
			foreach ($tList as $type) {
				if ($type->getName() == $moduleName) $allowedTypes[] = $type->getExt();
			}
			$inputData = Array('element' => $element, 'allowed-element-types' => $allowedTypes);
	
			if ($mode == "do") {
				$element = $this->saveEditedElementData($inputData);
				$this->chooseRedirect();
			}
	
			$this->setDataType("form");
			$this->setActionType("modify");
	
			$data = $this->prepareData($inputData, 'page');
	
			$this->setData($data);
			return $this->doData();
		}

		public function editObject() {
			$object = $this->expectObject("param0");
			$mode = (string)getRequest('param1');
			
			if ($mode == "do") {
				$this->saveEditedObjectData($object);
				$this->chooseRedirect();
			}
			
			$this->setDataType("form");
			$this->setActionType("modify");
			
			$data = $this->prepareData($object, "object");
			
			$this->setData($data);
			return $this->doData();
		}

		public function editType() {
			$object = $this->expectObjectType('param0');
			$mode = (string)getRequest('param1');
			$oModuleData = cmsController::getInstance()->getModule('data');
			
			if ($mode == "do") {
				$this->saveEditedTypeData($object);
				$this->chooseRedirect();
			}
			
			$this->setDataType("form");
			$this->setActionType("modify");
			
			$data = $this->prepareData($object, "type");
			
			$this->setData($data);
			return $oModuleData->type_edit();
		}


		public function delObject() {
			$objects = getRequest('element');
			if (!is_array($objects)) $objects = Array($objects);
	
			foreach ($objects as $objectId) {
				$object = $this->expectObject($objectId, false, true);
				$params = Array('object' => $object);
				$this->deleteObject($params);
			}
	
			$this->setDataType("list");
			$this->setActionType("view");
			$data = $this->prepareData($objects, "objects");
			$this->setData($data);
	
			return $this->doData();
		}
	
	
		public function activityObject() {
			$objects = getRequest('object');
			if (!is_array($objects)) $objects = Array($objects);
			
			$is_active = (bool)getRequest('active');
			
			foreach ($objects as $objectId) {
				$object = $this->expectObject($objectId, false, true);
				$object->setValue("is_active", $is_active);
				$object->commit();
			}
			
			$this->setDataType("list");
			$this->setActionType("view");
			$data = $this->prepareData($objects, "objects");
			$this->setData($data);
			
			return $this->doData();
		}
		/* need for "type" modification template futures */
		public function importDataModuleFormTemplate() {
			$cmsController = cmsController::getInstance();
			$skin = system_get_skinName();
			$path = CURRENT_WORKING_DIR . '/styles/skins/' . $skin . '/data/modules/data/form.modify.xsl';
			return array('plain:result' => file_get_contents($path));
		}
		/* need for modern skin form form.modify */
		public function getTypeNameByTypeId($typeId){
			$type = umiObjectTypesCollection::getInstance()->getType($typeId);
			if ($type instanceof umiObjectType) return $type->getMethod();
			$type = reset(umiHierarchyTypesCollection::getInstance()->getTypesByModules(get_class($this)));
			if ($type instanceof umiObjectType) return $type->getMethod();
		}
		/* not remember */
		public function getTypeIdByHierarchyTypeName($method){
			$typeId = umiObjectTypesCollection::getInstance()->getTypeIdByHierarchyTypeName(get_class($this), $method);
			return $typeId;
		}

		public function getDatasetConfiguration($params) {
			$params		= explode (',' , $params);
			$moduleName = $module = cmsController::getInstance()->getCurrentModule();
			$eType		= $params[0]; 
			$hType		= $params[1]; 
			$loadMethod	= $params[2]; 
			
			$typeId	= umiObjectTypesCollection::getInstance()->getBaseType($moduleName, $hType);

			switch($eType) {
				case 'pages' :
					$moduleName		= 'content';
					$delMethod		= 'tree_delete_element';
					$activityMethod	= 'tree_set_activity';
					$defaults	= array();
					break;
				case 'objects' :
					$delMethod		= 'delObject';
					$activityMethod	= 'activityObject';
					$defaults	= array();
					break;
				case 'types' : 
					$moduleName = 'data';
					$delMethod  = 'type_del';
					$activityMethod	= 'activity';
					$defaults	= array();
					break;
				default:
					break;
			}		
			return array(
					'methods' => array(
						array('title'=>getLabel('smc-load'),		'module'=>$module,		'#__name'=>$loadMethod, 'forload'=>true),
						array('title'=>getLabel('smc-delete'),		'module'=>$moduleName,	'#__name'=>$delMethod, 'aliases' => 'tree_delete_element,delete,del'),
						array('title'=>getLabel('smc-activity'),	'module'=>$moduleName,	'#__name'=>$activityMethod, 'aliases' => 'tree_set_activity,activity'),
						array('title'=>getLabel('smc-copy'),		'module'=>'content',	'#__name'=>'tree_copy_element')),
					'types' => array(
						array('common' => 'true', 'id' => $typeId)
					),
					'stoplist' => array(''),
					'default' => $defaults
			);
		}			
	};
?>