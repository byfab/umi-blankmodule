<?php
	abstract class __frontend_blankModule {
		// Базовый метод вывода элементов
		public function getElementList($path = false, $limit = false) {
			$category_id = $this->analyzeRequiredPath($path);
			if (!$category_id) throw new publicException(getLabel('error-page-does-not-exist', null, $path));

			if (!$limit) $limit = $this->per_page;
			$curr_page = (int) getRequest('p');
			$offset = $limit * $curr_page;
			
			$sel = new selector('pages');
			$sel->types('hierarchy-type')->name(get_class($this), 'blank');
			$sel->where('hierarchy')->page($category_id)->childs(1);
			$sel->where('is_active')->equals(true);		// Активная
			$sel->where('is_deleted')->equals(false);	// Удалена, в корзине
			$sel->where('is_visible')->equals(false);	// Отображать в меню
			$sel->limit($offset, $limit);	
			selectorHelper::detectFilters($sel);	
			$sel->result();
			$total = $sel->length();

			$aItems = array();
			foreach ($sel as $object) {
				$aItem = array();
				$aItem['attribute:id'] = $object->id;
				$aItem['attribute:name'] = $object->name;
				$aItem['attribute:link'] = $object->link;
				$aItems[] = def_module::parseTemplate('', $aItem, $object->id);
			}
			$Output['items']['nodes:item'] = $aItems;
			$Output['total'] = $total;
			$Output['per_page'] = $limit;
			return def_module::parseTemplate('', $Output);
		}

		// Базовый метод вывода Обьектов
		public function getObjectList($path = false, $limit = false) {
			$category_id = $this->analyzeRequiredPath($path);
			if (!$category_id) throw new publicException(getLabel('error-page-does-not-exist', null, $path));

			if (!$limit) $limit = $this->per_page;
			$curr_page = (int) getRequest('p');
			$offset = $limit * $curr_page;
			
			$sel = new selector('objects');
			$sel->types('object-type')->name(get_class($this), 'blank');
			$sel->where('is_active')->equals(true);
			$sel->limit($offset, $limit);	
			selectorHelper::detectFilters($sel);	
			$sel->result();
			$total = $sel->length();

			foreach ($sel as $object) {
				$aItem = array();
				$aItem['attribute:id'] = $object->id;
				$aItem['attribute:name'] = $object->name;
				$aItems[] = def_module::parseTemplate('', $aItem, $object->id);
			}
			$Output['items']['nodes:item'] = $aItems;
			$Output['total'] = $total;
			$Output['per_page'] = $limit;
			return def_module::parseTemplate('', $Output);
		}
	};
?>