<?php
	chdir($_SERVER['DOCUMENT_ROOT']);
	define("MODULE_NAME", "blankModule");
	define("PATH_FOR_RESTRICTIONS", "./classes/system/subsystems/models/data/restrictions/");
	define('PATH_FOR_ADMIN_IMAGES_MAC', './images/cms/admin/mac/icons');
	define('PATH_FOR_ADMIN_IMAGES_MODERN', './images/cms/admin/modern');

	error_reporting(E_ALL);
	include "standalone.php";
	$destinationPath = CURRENT_WORKING_DIR . '/classes/modules/' . MODULE_NAME . '/export/'; // Задаём директорию для сохранения файлов экспорта.
	if (!is_dir($destinationPath)) mkdir($destinationPath, 0777);

	$exporter = new xmlExporter(MODULE_NAME); // Задаём идентификатор экспорта
	$exporter->setDestination($destinationPath); // Задаём директорию для сохранения файлов экспорта

	/* Функция для рекурсивного выбора файлов из директорий */
	function recursiveGlob($pattern = '*', $flags = 0, $path = '') {
		$paths = glob($path . '*', GLOB_MARK|GLOB_ONLYDIR|GLOB_NOSORT);
		$files = glob($path . $pattern, $flags);
		foreach ($paths as $path) { 
			if (strstr($path, '/classes/modules/' . MODULE_NAME . '/export/')) continue; 
			$files=array_merge($files, recursiveGlob($pattern, $flags, $path)); 
		}
		return $files;
	}


	$files['php'] = recursiveGlob('*.*', 0, './classes/modules/' . MODULE_NAME . '/');
	//$files['restrictions'] = array(PATH_FOR_RESTRICTIONS . 'updateTime.php');
	$files['ico'] = array( PATH_FOR_ADMIN_IMAGES_MAC . '/big/' . MODULE_NAME . '.png',
						PATH_FOR_ADMIN_IMAGES_MAC . '/medium/' . MODULE_NAME . '.png',
						PATH_FOR_ADMIN_IMAGES_MAC . '/small/' . MODULE_NAME . '.png',
						PATH_FOR_ADMIN_IMAGES_MODERN . '/icon/' . MODULE_NAME . '.png');
	$files['admin_templates_mac'] = recursiveGlob('*.xsl', 0, './styles/skins/mac/data/modules/' . MODULE_NAME);
	$files['admin_templates_modern'] = recursiveGlob('*.xsl', 0, './styles/skins/modern/data/modules/' . MODULE_NAME);
	/* Выбираем файлы модуля */


	$export_files = array();
	array_walk($files, function ($value) use (&$export_files) {
		$export_files = array_merge($export_files, $value);
	});

	$exporter->addFiles($export_files); // Задаём массив экспортируемых файлов

	/* Функция для рекурсивного выбора ключей реестра */
	function getAllRegistryList ($parent_path) {
		$paths = array();
		$children = regedit::getInstance()->getList($parent_path);
		if (is_array($children)) {
			foreach ($children as $child) {
				$child_key = regedit::getInstance()->getKey($child[0]);
				$child_path = ($parent_path != '//') ? $parent_path . '/' . $child[0] : $child[0];
				$paths[] = $child_path;
				$paths = array_merge($paths, getAllRegistryList($child_path));
			}
		}
		return $paths;
	}
	/* Выбираем пути к записям реестра */
	$paths = getAllRegistryList ('//modules/' . MODULE_NAME);
	$exporter->addRegistry($paths); // Задаём массив путей к записям реестра


	$hTypes = umiHierarchyTypesCollection::getInstance()->getTypesList();
	$types = array();
	array_walk($hTypes, function ($value, $key) use(&$types){
		if ($value->getName() == MODULE_NAME) $types = array_merge($types, array_keys(umiObjectTypesCollection::getInstance()->getTypesByHierarchyTypeId($key)));
	});
	$exporter->addTypes($types); // Задаём массив id типов


	/* Выбираем страницы и объекты, относящиеся к выбранным выше типам данных */
	foreach ($types as $typeId) {
		$sel = new selector("pages");
		$sel->types('object-type')->id($typeId);
		$pages = $sel->result();
		$exporter->addElements($pages); // Задаём массив элементов

		$sel = new selector("objects");
		$sel->types('object-type')->id($typeId);
		$objects = $sel->result();
		$exporter->addObjects($objects); // Задаём массив объектов
	}

	$saveRelations = array('files', 'templates', 'objects', 'fields_relations', 'restrictions', 'hierarchy', 'guides');
	$exporter->setIgnoreRelations($saveRelations);
	$exporter->setShowAllFields(true); // Устанавливаем флаг экспорта всех полей (в т.ч. системных и скрытых)
	$dom = $exporter->execute(); // Запускаем экспорт. Результат записываем в переменную
	$log = $exporter->getExportLog(); // В этой переменной хранится лог ошибок экспорта (нужна для отладки)

print_r($log);
	/* Сохраняем xml для будущего импорта */
	file_put_contents($destinationPath . MODULE_NAME . '.xml', $dom->saveXML());
 ?>