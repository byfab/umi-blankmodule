<?php

/**
 * Установщик модуля PHP 5.6
 */

$module_name = 'blankModule';
$module_path = './classes/modules/' . $module_name;

/**
 * @var array $INFO реестр модуля
 */
$INFO = Array(
	'name' => $module_name,                         // Имя модуля
	'filename' => $module_name . '/' . 'class.php', // Путь к класу модуля
	'config' => "1",                                // У модуля есть настройки
	'default_method' => "item",                     // Метод по умолчанию в клиентской части
	'default_method_admin' => "category",           // Метод по умолчанию в административной части
	'func_perms' => array(                          // Группы прав
		'guest' => "Гостевые права",                // Гостевая группа прав
		'admin' => "Административные права"         // Административная группа прав
	),
	'per_page' => "10"  //Настройка количества выводимых страниц (будем использовать в конфигурации)
);


/**
 * @var array $COMPONENTS файлы модуля
 */
$COMPONENTS = array(
	$module_path . '/lang.php',         // Языковые константы
	$module_path . '/i18n.php',         // Язык по умолчанию
	$module_path . '/i18n.en.php',      // Английский язык
	$module_path . '/permissions.php',  // Права
	$module_path . '/__admin.php',      // Макросы административной части
	$module_path . '/__custom_adm.php',
	$module_path . '/class.php',        // Класс модуля
	$module_path . '/macros.php',       // Макросы клиентской части
	$module_path . '/__custom.php',
);

?>