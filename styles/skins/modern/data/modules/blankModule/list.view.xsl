<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://common/blankModule" [

]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:umi="http://www.umi-cms.ru/TR/umi">



	<!-- Pages -->
	<xsl:template match="/result[@method = 'category']/data">
		<!-- Common module params -->
		<xsl:variable name="eType" select="string('pages')" />	<!-- pages/objects/types -->
		<xsl:variable name="hType" select="string('item')" />	<!-- atomar hType need for add button in content module-->
		<xsl:variable name="params"><xsl:value-of select="$eType" />,<xsl:value-of select="$hType" />,<xsl:value-of select="/result/@method" /></xsl:variable>	<!-- or some custom, if set custom, need modify getDatasetConfiguration -->
		<!-- addButton params -->
		<div class="location">
			<div class="imgButtonWrapper">
				<a href="#" umi:type="{/result/@module}::category" umi:activity="true,category" class="btn color-blue loc-left">Добавить категорию</a>
				<a href="#" umi:type="{/result/@module}::item" umi:prevent-default="true" umi:activity="category" class="btn color-blue loc-left">Добавить обьект</a>
			</div>
			<a class="btn-action loc-right infoblock-show"><i class="small-ico i-info"></i><xsl:text>&help;</xsl:text></a>
		</div>
		<!-- //addButton params -->
		<div class="layout">
			<div class="column">
				<xsl:call-template name="ui-smc-table">
					<xsl:with-param name="content-type"		select="$eType" />
					<xsl:with-param name="control-params"	select="$params" />
					<xsl:with-param name="allow-drag"		select="'1'" />
					<xsl:with-param name="js-add-buttons">
						$('.imgButtonWrapper > .btn').each(function( index ) {
							var aModuleMethod = $(this).attr('umi:type').split('::', 2);
							var link = '<xsl:value-of select="/result/@pre-lang" />/admin/' + aModuleMethod[0] + '/addElement/' + '{id}' + '/' + aModuleMethod[1] + '/';
							var activityRules = $(this).attr('umi:activity').split(',');
							for(i = 0; i &lt; activityRules.length; i++) {
								activityRules[i] = (activityRules[i] == 'true') ? true : activityRules[i];
							}
							createAddButton(this, oTable, link, activityRules);
							//this can be some custom JS, for modification button
						});
					</xsl:with-param>
				</xsl:call-template>
			</div>
			<div class="column">
				<div  class="infoblock">
					<h3><xsl:text>&label-quick-help;</xsl:text></h3>
					<div class="content" title="{$context-manul-url}"></div>
					<div class="infoblock-hide"></div>
				</div>
			</div>
		</div>
	</xsl:template>

	<!-- Objects -->
	<xsl:template match="/result[@method = 'objects']/data">
		<!-- Common module params -->
		<xsl:variable name="eType" select="string('objects')" />	<!-- pages/objects/types -->
		<xsl:variable name="hType" select="string('object')" />		<!-- atomar hType need for add button in content module-->
		<xsl:variable name="params"><xsl:value-of select="$eType" />,<xsl:value-of select="$hType" />,<xsl:value-of select="/result/@method" /></xsl:variable>	<!-- or some custom, if set custom, need modify getDatasetConfiguration -->
		<!-- addButton params -->
		<div class="location">
			<div class="imgButtonWrapper loc-left">
				<a class="type_select btn color-blue" umi:type="{/result/@module}::object" umi:activity="true,*" href="#">Обьект</a>
			</div>
			<a class="btn-action loc-right infoblock-show"><i class="small-ico i-info"></i><xsl:text>&help;</xsl:text></a>
		</div>
		<!-- //addButton params -->
		<div class="layout">
			<div class="column">
				<xsl:call-template name="ui-smc-table">
					<xsl:with-param name="content-type"		select="$eType" />
					<xsl:with-param name="control-params"	select="$params" />
					<xsl:with-param name="enable-objects-activity">1</xsl:with-param>
					<xsl:with-param name="js-add-buttons">
						$('.imgButtonWrapper a').each(function( index ) {
							var aModuleMethod = $(this).attr('umi:type').split('::', 2);
							var link = '<xsl:value-of select="/result/@pre-lang" />/admin/' + aModuleMethod[0] + '/addObject/' + aModuleMethod[1] + '/';
							var activityRules = $(this).attr('umi:activity').split(',');
							for(i = 0; i &lt; activityRules.length; i++) {
								activityRules[i] = (activityRules[i] == 'true') ? true : activityRules[i];
							}
							createAddButton(this, oTable, link, activityRules);
							//this can be some custom JS, for modification button
						});
					</xsl:with-param>
				</xsl:call-template>
			</div>
			<div class="column">
				<div  class="infoblock">
					<h3><xsl:text>&label-quick-help;</xsl:text></h3>
					<div class="content" title="{$context-manul-url}"></div>
					<div class="infoblock-hide"></div>
				</div>
			</div>
		</div>
	</xsl:template>

	<!-- Types -->
	<xsl:template match="/result[@method = 'types']/data">
		<!-- Common module params -->
		<xsl:variable name="eType" select="string('types')" />	<!-- pages/objects/types -->
		<xsl:variable name="hType" select="string('type')" />	<!-- atomar hType need for add button in content module-->
		<xsl:variable name="params"><xsl:value-of select="$eType" />,<xsl:value-of select="$hType" />,<xsl:value-of select="/result/@method" /></xsl:variable>	<!-- or some custom, if set custom, need modify getDatasetConfiguration -->
		<!-- addButton params -->	
		<div class="location">
			<div class="imgButtonWrapper loc-left">
				<a class="type_select btn color-blue" umi:basetype="{/result/@module}::blanktype" umi:typeId="121" umi:activity="true,blanktype" href="#">Тип</a>
				<a class="type_select btn color-blue" umi:basetype="{/result/@module}::anotherblanktype" umi:typeId="121" umi:activity="blanktype" href="#">Тип 2</a>
			</div>
			<a class="btn-action loc-right infoblock-show"><i class="small-ico i-info"></i><xsl:text>&help;</xsl:text></a>
		</div>
		<!-- //addButton params -->
		<div class="layout">
			<div class="column">
				<xsl:call-template name="ui-smc-table">
					<xsl:with-param name="content-type"		select="$eType" />
					<xsl:with-param name="control-params"	select="$params" />
					<xsl:with-param name="disable-name-filter">true</xsl:with-param>
					<xsl:with-param name="enable-edit">false</xsl:with-param>
					<xsl:with-param name="js-add-buttons">
						$('.imgButtonWrapper a').each(function( index ) {
							var aModuleMethod = $(this).attr('umi:basetype').split('::', 2);
							var typeId = $(this).attr('umi:typeId');
							var link = '<xsl:value-of select="/result/@pre-lang" />/admin/' + aModuleMethod[0] + '/addType/' + '{id}' + '/' + aModuleMethod[1] + '/';
							var activityRules = $(this).attr('umi:activity').split(',');
							for(i = 0; i &lt; activityRules.length; i++) {
								activityRules[i] = (activityRules[i] == 'true') ? true : activityRules[i];
							}
		
							//oTable.selectedList[typeId] = { baseMethod : true }; //TODO root type id where page loaded
							createAddButton(this, oTable, link, activityRules);
							//oTable.selectedList = '';
							//this can be some custom JS, for modification button
						});
					</xsl:with-param>					
					<!-- Калбэк для каждого элемента таблицы -->
					<xsl:with-param name="js-value-callback"><![CDATA[
						function (value, name, item) {
							var data = item.getData(); //xml объекта
							item.baseModule = data.base.module;
							item.baseMethod = data.base.method;
							return data.title;
						}
					]]></xsl:with-param>
					<xsl:with-param name="menu"><![CDATA[
						var menu = [
							['edit-item',        'ico_edit', ContextMenu.itemHandlers.editItem],
							['delete',           'ico_del',  ContextMenu.itemHandlers.deleteItem]
						]
					]]></xsl:with-param>
				</xsl:call-template>
			</div>
			<div class="column">
				<div  class="infoblock">
					<h3><xsl:text>&label-quick-help;</xsl:text></h3>
					<div class="content" title="{$context-manul-url}"></div>
					<div class="infoblock-hide"></div>
				</div>
			</div>
		</div>
	</xsl:template>	
	
	<xsl:template match="result" mode="tabs">
		<xsl:if test="count($tabs/items/item)">
			<div class="tabs module"><xsl:apply-templates select="$tabs" /></div>
		</xsl:if>
		<div class="tabs-content module"><div class="section selected"><xsl:apply-templates select="." /></div></div>
	</xsl:template>	
	

</xsl:stylesheet>