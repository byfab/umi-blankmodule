<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common" [

]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:umi="http://www.umi-cms.ru/TR/umi">
	<xsl:output encoding="utf-8" />

	<!-- Pages -->
	<xsl:template match="/result[@method = 'category']/data[@type = 'list' and @action = 'view']">
		<!-- Common module params -->
		<xsl:variable name="eType" select="string('pages')" />	<!-- pages/objects/types -->
		<xsl:variable name="hType" select="string('item')" /> <!-- atomar hType need for add button in content module-->
		<xsl:variable name="params"><xsl:value-of select="$eType" />,<xsl:value-of select="$hType" />,<xsl:value-of select="/result/@method" /></xsl:variable>	<!-- or some custom, if set custom, need modify getDatasetConfiguration -->
		<!-- addButton params -->
		<div class="imgButtonWrapper">
			<a class="type_select_gray" umi:type="{/result/@module}::category" umi:activity="true,category" href="#">Категорию</a>
			<a class="type_select_gray" umi:type="{/result/@module}::item" umi:activity="category" href="#">Обьект</a>
		</div>

		<xsl:call-template name="ui-smc-table">
			<xsl:with-param name="content-type"		select="$eType" />
			<xsl:with-param name="control-params"	select="$params" />
			<xsl:with-param name="js-ignore-props-edit">['name', 'sending_time']</xsl:with-param>
			<xsl:with-param name="js-add-buttons">
				$('.imgButtonWrapper a').each(function( index ) {
					var aModuleMethod = $(this).attr('umi:type').split('::', 2);
					var link = '<xsl:value-of select="/result/@pre-lang" />/admin/' + aModuleMethod[0] + '/addElement/' + '{id}' + '/' + aModuleMethod[1] + '/';
					var activityRules = $(this).attr('umi:activity').split(',');
					for(i = 0; i &lt; activityRules.length; i++) {
						activityRules[i] = (activityRules[i] == 'true') ? true : activityRules[i];
					}
					createAddButton(this, oTable, link, activityRules);
					//this can be some custom JS, for modification button
				});
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>

	<!-- Objects -->
	<xsl:template match="/result[@method = 'objects']/data[@type = 'list' and @action = 'view']">
		<!-- Common module params -->
		<xsl:variable name="eType" select="string('objects')" />	<!-- pages/objects/types -->
		<xsl:variable name="hType" select="string('object')" /> <!-- atomar hType need for add button in content module-->
		<xsl:variable name="params"><xsl:value-of select="$eType" />,<xsl:value-of select="$hType" />,<xsl:value-of select="/result/@method" /></xsl:variable>	<!-- or some custom, if set custom, need modify getDatasetConfiguration -->
		<!-- addButton params -->
		<div class="imgButtonWrapper">
			<a class="type_select_gray" umi:type="{/result/@module}::object" umi:activity="true,*" href="#">Обьект</a>
		</div>

		<xsl:call-template name="ui-smc-table">
			<xsl:with-param name="content-type"		select="$eType" />
			<xsl:with-param name="control-params"	select="$params" />
			<xsl:with-param name="enable-objects-activity">1</xsl:with-param> <!-- Активен [is_active] видимое -->
			<xsl:with-param name="flat-mode">1</xsl:with-param>
			
			<xsl:with-param name="js-ignore-props-edit">['name', 'sending_time']</xsl:with-param>
			<xsl:with-param name="js-add-buttons">
				$('.imgButtonWrapper a').each(function( index ) {
					var aModuleMethod = $(this).attr('umi:type').split('::', 2);
					var link = '<xsl:value-of select="/result/@pre-lang" />/admin/' + aModuleMethod[0] + '/addObject/' + aModuleMethod[1] + '/';
					var activityRules = $(this).attr('umi:activity').split(',');
					for(i = 0; i &lt; activityRules.length; i++) {
						activityRules[i] = (activityRules[i] == 'true') ? true : activityRules[i];
					}
					createAddButton(this, oTable, link, activityRules);
					//this can be some custom JS, for modification button
				});
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>

	<!-- Types -->
	<xsl:template match="/result[@method = 'types']/data[@type = 'list' and @action = 'view']">
		<!-- Common module params -->
		<xsl:variable name="eType" select="string('types')" />	<!-- pages/objects/types -->
		<xsl:variable name="hType" select="string('type')" /> <!-- atomar hType need for add button in content module-->
		<xsl:variable name="params"><xsl:value-of select="$eType" />,<xsl:value-of select="$hType" />,<xsl:value-of select="/result/@method" /></xsl:variable>	<!-- or some custom, if set custom, need modify getDatasetConfiguration -->
		<!-- addButton params -->
		<div class="imgButtonWrapper">
			<xsl:call-template name="entity-add-button">
				<xsl:with-param name="title">Тип</xsl:with-param>
				<xsl:with-param name="name">blanktype</xsl:with-param>
				<xsl:with-param name="activity">true,blanktype</xsl:with-param>
			</xsl:call-template>
			<xsl:call-template name="entity-add-button">
				<xsl:with-param name="title">Тип 2</xsl:with-param>
				<xsl:with-param name="name">anotherblanktype</xsl:with-param>
				<xsl:with-param name="activity">blanktype</xsl:with-param>
			</xsl:call-template>
		</div>
		<xsl:call-template name="ui-smc-table">
			<xsl:with-param name="content-type"		select="$eType" />
			<xsl:with-param name="control-params"	select="$params" />
			<xsl:with-param name="flat-mode">0</xsl:with-param>
			<xsl:with-param name="js-add-buttons">
				$('.imgButtonWrapper a').each(function( index ) {
					var aModuleMethod = $(this).attr('umi:basetype').split('::', 2);
					var typeId = $(this).attr('umi:typeId');
					var link = '<xsl:value-of select="/result/@pre-lang" />/admin/' + aModuleMethod[0] + '/addType/' + '{id}' + '/' + aModuleMethod[1] + '/';
					var activityRules = $(this).attr('umi:activity').split(',');
					for(i = 0; i &lt; activityRules.length; i++) {
						activityRules[i] = (activityRules[i] == 'true') ? true : activityRules[i];
					}

					oTable.selectedList[typeId] = { baseMethod : true };
					createAddButton(this, oTable, link, activityRules);
					oTable.selectedList = '';
					//this can be some custom JS, for modification button
				});
			</xsl:with-param>
			<!-- Калбэк для каждого элемента таблицы -->
			<xsl:with-param name="js-value-callback"><![CDATA[
				function (value, name, item) {
					var data = item.getData(); //xml объекта
					item.baseModule = data.base.module;
					item.baseMethod = data.base.method;
					return data.title;
				}
			]]></xsl:with-param>
			<xsl:with-param name="menu"><![CDATA[
				var menu = [
					['edit-item',        'ico_edit', ContextMenu.itemHandlers.editItem],
					['delete',           'ico_del',  ContextMenu.itemHandlers.deleteItem]
				]
			]]></xsl:with-param>
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="entity-add-button">
		<xsl:param name="title" select="basetypes/basetype" />
		<xsl:param name="name" select="basetypes/basetype/@method" />
		<xsl:param name="activity">*</xsl:param>
		<xsl:param name="typeId" select="document(concat('udata://blankModule/getTypeIdByHierarchyTypeName/', $name))" />
		<a class="type_select_gray" umi:basetype="{/result/@module}::{$name}" umi:typeId="{$typeId}" umi:activity="{$activity}" href="#"><xsl:value-of select="$title" /></a>
	</xsl:template>

	<!-- FullPattern -->
	<xsl:template match="/result[@method = 'subscribers']/data[@type = 'list' and @action = 'view']">
		<!-- Common module params -->
		<xsl:variable name="eType" select="string('pages')" />	<!-- pages/objects/types -->
		<xsl:variable name="hType" select="string('trainer')" /> <!-- atomar hType need for add button in content module-->
		<xsl:variable name="params"><xsl:value-of select="$eType" />,<xsl:value-of select="$hType" />,<xsl:value-of select="/result/@method" /></xsl:variable>	<!-- or some custom, if set custom, need modify getDatasetConfiguration -->
		<!-- addButton params -->
		<div class="imgButtonWrapper">
			<a id="add_category" class="type_select_gray" umi:type="{/result/@module}::category" umi:activity="true,category" href="#">Категорию</a>
			<a id="add_item" class="type_select_gray" umi:type="{/result/@module}::item" umi:activity="category" href="#">Обьект</a>
		</div>

		<xsl:call-template name="ui-smc-table">
			<xsl:with-param name="content-type"		select="$eType" />
			<xsl:with-param name="control-params"	select="$params" />
			<xsl:with-param name="js-ignore-props-edit">['name', 'sending_time']</xsl:with-param>
			<xsl:with-param name="js-add-buttons">
				$('.imgButtonWrapper a').each(function( index ) {
					var aModuleMethod = $(this).attr('umi:type').split('::', 2);
					var link = '<xsl:value-of select="/result/@pre-lang" />/admin/' + aModuleMethod[0] + '/addElement/' + '{id}' + '/' + aModuleMethod[1] + '/';
					var activityRules = $(this).attr('umi:activity').split(',');
					for(i = 0; i &lt; activityRules.length; i++) {
						activityRules[i] = (activityRules[i] == 'true') ? true : activityRules[i];
					}
					createAddButton(this, oTable, link, activityRules);
					//this can be some custom JS, for modification button
				});
			</xsl:with-param>
		
		
			<!-- 
				content-type – указывает на то, по каким правилам необходимо визуально отобразить данные.
				Варианты значений: (types, objects, pages) 
			-->
			<xsl:with-param name="content-type">objects</xsl:with-param>
			<!-- 
				control-params  - это имя строкового параметра, который передается в getDatasetConfiguration()
				Во избежание путаницы, передаем имя метода для текущей вкладки
			 -->
			<xsl:with-param name="control-params">subscribers</xsl:with-param> 
			<!-- 
				enable-objects-activity - позволяет менять активность объекта (требуется поле is_activated, либо is_active в типе данных)
			 -->			
			<xsl:with-param name="enable-objects-activity">1</xsl:with-param>
			<!-- 
				search-show – Отображение формы поиска данных в модуле
			 -->
			<xsl:with-param name="search-show">1</xsl:with-param>
			<!-- 
				domains-show – этот параметр включает отображение выпадающего списка с доменами, в случае если их больше одного.
			 -->
			<xsl:with-param name="domains-show">0</xsl:with-param>
			<!-- 
				disable-csv-buttons – отображение функции экспорта/импорта данных в формате CSV, под списком объектов.
			 -->
			<xsl:with-param name="disable-csv-buttons">0</xsl:with-param>
			<!-- 
				flat-mode – табличное отображение данных, а не древовидное.
			 -->			
			<xsl:with-param name="flat-mode">1</xsl:with-param>
			<!-- 
				js-ignore-props-edit – Отключение возможности быстрого редактирования перечисленных полей
			 -->
			<xsl:with-param name="js-ignore-props-edit">['name', 'sending_time']</xsl:with-param>
			<!-- 
				js-value-callback – CallBack функция (полезно для вывода "типов данных")
			 -->			
			<!--<xsl:with-param name="js-value-callback"><![CDATA[
				function (value, name, item) {
					var data = item.getData();
					return data.title;
				}
			]]></xsl:with-param>-->		
		</xsl:call-template>
	</xsl:template>
	

</xsl:stylesheet>