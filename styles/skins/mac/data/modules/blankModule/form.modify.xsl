<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common" [
	<!ENTITY module	"blankModule">
]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:umi="http://www.umi-cms.ru/TR/umi">
	<xsl:include href="udata://&module;/importDataModuleFormTemplate/" /> <!-- need for "type" modification template futeres  -->

	<!-- Object and Page -->
	<xsl:template match="properties/group" mode="form-modify">
		<xsl:param name="show-name"><xsl:text>1</xsl:text></xsl:param>
		<xsl:param name="show-type"><xsl:text>1</xsl:text></xsl:param>

		<div class="panel properties-group" name="g_{@name}">
			<div class="header"><span><xsl:value-of select="@title" /></span><div class="l" /><div class="r" /></div>
			<div class="content">
				<xsl:apply-templates select="." mode="form-modify-group-fields">
					<xsl:with-param name="show-name" select="$show-name" />
					<xsl:with-param name="show-type" select="$show-type" />
				</xsl:apply-templates>
				<xsl:apply-templates select="ancestor::data/@action" mode="std-form-buttons" />
			</div>
		</div>
	</xsl:template>
	
	<xsl:template match="@action[current() = 'create']" mode="std-form-buttons">
		<div class="buttons">
			<div><input type="submit" value="&label-save-add;" name="save-mode" /><span class="l" /><span class="r" /></div>
			<div><input type="submit" value="&label-save-add-exit;" name="save-mode" /><span class="l" /><span class="r" /></div>
			<xsl:if test="/result/data/page"><div><input type="submit" value="&label-save-add-view;" name="save-mode" /><span class="l" /><span class="r" /></div></xsl:if>
			<div><input type="button" value="&label-cancel;" onclick="javascript: window.location = '{/result/@referer-uri}';" /><span class="l" /><span class="r" /></div>
		</div>
		<xsl:if test="/result/data/page"><script>jQuery('div.buttons input:submit').attr('disabled', 'disabled');</script></xsl:if>
	</xsl:template>

	<xsl:template match="@action[current() = 'modify']" mode="std-form-buttons">
		<div class="buttons">
			<div><input type="submit" value="&label-save;" name="save-mode" /><span class="l" /><span class="r" /></div>
			<div><input type="submit" value="&label-save-exit;" name="save-mode" /><span class="l" /><span class="r" /></div>
			<xsl:if test="/result/data/page"><div><input type="submit" value="&label-save-view;" name="save-mode" /><span class="l" /><span class="r" /></div></xsl:if>
			<div><input type="button" value="&label-cancel;" onclick="javascript: window.location = '{/result/@referer-uri}';" /><span class="l" /><span class="r" /></div>
		</div>
		<xsl:if test="/result/data/page"><script>jQuery('div.buttons input:submit').attr('disabled', 'disabled');</script></xsl:if>
	</xsl:template>
	<!-- //Object -->

	<!-- types -->
	<xsl:template match="/result[@method = 'addType' or @method = 'editType']/data[@type = 'form' and (@action = 'modify' or @action = 'create')]">
		<form action="do/" method="post" enctype="multipart/form-data">
			<input type="hidden" name="referer" value="{/result/@referer-uri}"/>
			<xsl:apply-templates select="type" mode="fieldgroup-common" />
		</form>
		<xsl:apply-templates select="//fieldgroups" />
	</xsl:template>

	<xsl:template match="type" mode="fieldgroup-common">
		<div class="panel properties-group">
			<div class="header"><span><xsl:text>&label-edit-type-common;</xsl:text></span><div class="l" /><div class="r" /></div>
			<div class="content">
				<div class="field">
					<label>
						<span class="label"><xsl:text>&label-type-name;</xsl:text></span>
						<span><input type="text" name="data[name]" value="{@title}" /></span>
					</label>
				</div>
				<div class="field" style="height:50px;">
					<label>
						<span class="label"><xsl:text></xsl:text></span>
						<xsl:variable name="base-id" select="base/@id"/>
						<span><input type="hidden" name="data[hierarchy_type_id]" value="{base/@id}" /></span>
					</label>
				</div>
				<div class="field">
					<label class="inline">
						<span class="label">
							<input type="hidden" name="data[is_public]" value="0" />
							<input type="checkbox" name="data[is_public]" value="1" class="checkbox"><xsl:if test="@public"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if></input>
							<xsl:text>&label-is-public;</xsl:text>
						</span>
					</label>
				</div>
				<div class="field">
					<label class="inline">
						<span class="label">
							<input type="hidden" name="data[is_guidable]" value="0" />
							<input type="checkbox" name="data[is_guidable]" value="1" class="checkbox"><xsl:if test="@guide"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if></input>
							<xsl:text>&label-is-guide;</xsl:text>
						</span>
					</label>
				</div>
				<xsl:apply-templates select="ancestor::data/@action" mode="std-form-buttons" />
			</div>
		</div>
	</xsl:template>

	<xsl:template match="result[@module = '&module;']//fieldgroups">
		<div class="panel properties-group">
			<div class="header"><span><xsl:text>&nbsp;</xsl:text></span><div class="l" /><div class="r" /></div>
			<div class="content">
				<xsl:apply-templates select="." mode="fieldsgroups-other" />
			</div>
		</div>	
	</xsl:template>
	<!-- //types -->

</xsl:stylesheet>